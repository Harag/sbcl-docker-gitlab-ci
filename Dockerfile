# Latest version of ubuntu official docker image
FROM ubuntu:rolling

# Installing what I need from ubuntu to do the job.
# - wget to download stuff from the web
#     -- curl gave me a 301 trying to download build app so I swiched to wget
# - sbcl and build-essentials - To build the version of sbcl downloaded
#     -- the sbcl in ubuntu is usually a bit dated that why we download what we want
# - libev-dev - is used by the woo http server that we are using for our example
#
RUN apt-get -y update && apt-get -y upgrade

# If we dont do this we get certificate errors when accessing git repositories with htts.
RUN  apt-get install -qy --no-install-recommends apt-transport-https ca-certificates  && \
update-ca-certificates
  
RUN DEBIAN_FRONTEND=noninteractive apt-get install -qy --no-install-recommends git sbcl curl build-essential time rlwrap

# Downloading, compiling and installing our prefered version of SBCL

RUN  cd /tmp && git clone git://git.code.sf.net/p/sbcl/sbcl

RUN cd /tmp/sbcl && \
sh ./make.sh  && \
sh ./install.sh

# Change dir before deleting it
RUN cd / && rm -rf /tmp/sbcl

#Used to clone local source into it and quicklisp is told about it
RUN mkdir /root/src

# Install quicklisp
RUN curl -k -o /tmp/quicklisp.lisp 'https://beta.quicklisp.org/quicklisp.lisp' && \
sbcl --noinform --non-interactive --load /tmp/quicklisp.lisp --eval \
'(quicklisp-quickstart:install :path "~/quicklisp/")' && \
sbcl --noinform --non-interactive --load ~/quicklisp/setup.lisp --eval \
'(ql-util:without-prompting (ql:add-to-init-file))' && \
echo '#+quicklisp(push "/root/src" ql:*local-project-directories*)' >> ~/.sbclrc

#Cleanup.
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN apt-get autoremove --purge && apt-get clean



