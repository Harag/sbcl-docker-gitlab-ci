app_dir := $(dir $(CURDIR))

all: container

container:
  sudo docker build -t sbcl-docker-git-lab-ci .

run: container
  sudo docker run -a stdin -a stdout -a stderr -i -t sbcl-docker-git-lab-ci

clean:
  sudo docker rm $(docker ps -a -q)
  sudo docker rmi $(docker images -q)
