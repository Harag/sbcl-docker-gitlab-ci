* sbcl-docker-gitlab-ci

A docker image specifically for use by gitlab CI for testing.

The docker image is build by gitlab CI and pushed to the gitlab docker
registry, nl registry.gitlab.com.

From the registry.gitlab.com you can then use the image in your
projects just point the CI at it like

: image: registry.gitlab.com/[username]]/[docker-project-name]

This docker image is not a work of art, it is basic and functional, an
expert would most likely do better! The same can be said for the
script that builds the docker image.

** Notes

The image uses the latest ubuntu release and clones sbcl compiling it
for use. [Could most probably remove the ubuntu version of sbcl used
to compile the cloned source code as well to save a drop of space.]

For the curious the time and the certificate shenanigan in the
dockerfile is to enable cloning of gitlab projects over https in the
docker image when you use it in your projects.

** Usage

I am not sure if gitlab.com will allow the use of the image in this
project directly, and who knows where the actual image is physically
stored, my bet is that it would be better of in your own project.

So to use this create your own project to host the files changing
names in makefile and dockerfile and gitlab-ci.yml file.

When you push those files to your project it will build the docker
image and publish it.

Then in your projects where you want to use the image for CI just
include the following .gitlab-ci.yml file once again filling in names
where needed and specifying your tests (look for [...] in the
script). Just including it in your project will kick of CI in
gitlab.com

- Note lines where wrapped for display purposes, you may have to fix that.

#+BEGIN_EXAMPLE
image: registry.gitlab.com/harag/sbcl-docker-gitlab-ci

stages:
  - test

before_script:
  - cd ~/src
  - git clone https://gitlab-ci-token:$CI_JOB_TOKEN@gitlab.com/[user-name]/[docker-project-name].git

test:
  stage: test
  script:
    - /usr/local/bin/sbcl --noinform --non-interactive
    --eval '(ql:quickload :[project tests])'
    --eval '
    (if ([run tests])
      (sb-ext:exit :code 0)
      (sb-ext:exit :code 200))'

#+END_EXAMPLE
